#!/bin/env python3
# -*- coding: utf-8 -*-

import logging

logging.basicConfig(filename='runtime.log', filemode='w', level=logging.DEBUG)

from game_engine import GameEngine

def main():
    engine = GameEngine()
    try:
        engine.run()
    except KeyboardInterrupt:
        pass
    engine.savegame.close()

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main()

#  vim: set tabstop=4 shiftwidth=4 expandtab autoindent :
