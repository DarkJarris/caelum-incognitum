# -*- coding: utf-8 -*-

"""
Interface definition for the generic UI system.
"""

from enum import Enum


class UserAction(Enum):
    """
    Enumeration of every UI-supported user actions.

    Argument to use when building a GameUserInterface.get_user_actions tuple:
        * UserAction.move:
            direction: angle in degrees of the movement vector,
                with 0 being going 'up' in a 2d, top-down view.
    """

    move = 0


class GameUserInterface(object):
    """
    Any UI implementation should inherit from this Class and implement
    the methods referenced here.

    :param ui_name: Canonical name of the UI system (SDL, urwid, etc...)
    :type ui_name: str
    """

    ui_name = None

    def __init__(self, ui_name, *_, **__):
        self.ui_name = ui_name

    def update(self, *_, **__):
        """
        Method for refreshing the UI.

        Called at least every game tick by the game's Engine.
        Processes user inputs as well as refreshes the GUI.
        """

        raise NotImplementedError

    def get_user_actions(self, *_, **__):
        """
        Method for fetching processed user inputs.

        Called at least every game tick by the game's Engine.
        Returns a list of actions submitted by the user and to be executed
        by the Engine.

        :returns: A list of tuples of the signature: (UserAction,
                                                     user_action_args,
                                                     user_action_kwargs)
        :rtype: list
        """

        raise NotImplementedError

    def shutdown(self, *_, **__):
        """
        Method for cleaning up the UI before destruction.

        Called when switching UIs or when closing the game.
        """
        raise NotImplementedError

__all__ = ['GameUserInterface', 'UserAction']

#  vim: set tabstop=4 shiftwidth=4 expandtab autoindent :
