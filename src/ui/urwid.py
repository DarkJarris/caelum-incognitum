#!/bin/env python3
# -*- coding: utf-8 -*-

"""
GameUserInterface implementation using the
Urwid console user interface library.
"""

import logging
import asyncio
import signal
import threading
import types
import urwid

from game_map import Pos
from . import GameUserInterface, UserAction


class UrwidEventLoopPolicy(asyncio.AbstractEventLoopPolicy):
    """
    Custom Asyncio loop policy to allow for use in parallel with the engine's
    loop.
    """
    _event_loop = None

    def get_child_watcher(self):
        pass

    def set_child_watcher(self, _):
        pass

    def get_event_loop(self):
        if UrwidEventLoopPolicy._event_loop is None:
            UrwidEventLoopPolicy._event_loop = asyncio.SelectorEventLoop()
        return UrwidEventLoopPolicy._event_loop

    def set_event_loop(self, loop):
        UrwidEventLoopPolicy._event_loop = loop

    def new_event_loop(self):
        loop = asyncio.SelectorEventLoop()
        self.set_event_loop(loop)


class PlayerSightCanvas(urwid.Canvas):
    """
    Urwid Canvas for displaying the player's view of the game map.
    """
    engine = None

    def __init__(self, engine, *args, **kwargs):
        self.engine = engine
        urwid.Canvas.__init__(self, *args, **kwargs)
        self.size = (self.engine.player.line_of_sight * 2 + 1,
                     self.engine.player.line_of_sight * 2 + 1)
        self.cursor = None

    def content_delta(self):
        pass

    def cols(self):
        return self.size[0]

    def rows(self):
        return self.size[1]

    def content(self, trim_left=0, trim_top=0, cols=None, rows=None, *_, **__):
        if cols is None:
            cols = self.size[0]
        if rows is None:
            rows = self.size[1]
        for row_num in range(self.engine.player.line_of_sight,
                             -self.engine.player.line_of_sight - 1,
                             -1):
            row = list()
            for col_num in range(self.engine.player.line_of_sight,
                                 -self.engine.player.line_of_sight - 1,
                                 -1):
                if col_num == 0 and row_num == 0:
                    tile = (None, None, b'@')
                else:
                    map_x = self.engine.player.pos.x_pos - col_num
                    map_y = self.engine.player.pos.y_pos - row_num
                    tile = self.engine.game_map[Pos(map_x, map_y)].model
                row.append(tile)
            yield row



class MapWidget(urwid.Widget):
    """
    Urwid Widget for displaying the game map as well as handling user input.
    """

    _sizing = frozenset(['fixed'])
    _selectable = True
    engine = None
    user_interface = None

    def __init__(self, engine, user_interface, *args, **kwargs):
        self.engine = engine
        self.user_interface = user_interface
        super(MapWidget).__init__(*args, **kwargs)

    def pack(self, *_, **__):
        return (self.engine.player.line_of_sight * 2 + 1,
                self.engine.player.line_of_sight * 2 + 1)

    def render(self, *_, **__):
        """Rendering handler."""

        return PlayerSightCanvas(engine=self.engine)

    def invalidate(self):
        """Mark the widget as stale for re-rendering."""

        self._invalidate()

    def keypress(self, _, key):
        """Handles user input."""

        if key == 'up':
            self.user_interface.player_direction = 0
            self._invalidate()
        elif key == 'down':
            self.user_interface.player_direction = 180
            self._invalidate()
        elif key == 'left':
            self.user_interface.player_direction = 270
            self._invalidate()
        elif key == 'right':
            self.user_interface.player_direction = 90
            self._invalidate()
        elif key == '+':
            self.engine.heartbeat_delay = 0.00
        elif key == '-':
            self.engine.heartbeat_delay = 0.04
        return key


class UrwidUserInterface(GameUserInterface):
    """Urwid-based implementation of the GameUserInterface.

    Runs a dedicated thread for a separate AsyncioEventLoop."""

    heartbeat_delay = 0.016
    rendering_thread = None
    engine = None
    palette = [
        ('body', 'black', 'light gray', 'standout'),
        ('header', 'white', 'dark red', 'bold'),
        ('screen edge', 'light blue', 'dark cyan'),
        ('main shadow', 'dark gray', 'black'),
        ('line', 'black', 'light gray', 'standout'),
        ('bg background', 'light gray', 'black'),
        ('bg 1', 'black', 'dark blue', 'standout'),
        ('bg 1 smooth', 'dark blue', 'black'),
        ('bg 2', 'black', 'dark cyan', 'standout'),
        ('bg 2 smooth', 'dark cyan', 'black'),
        ('button normal', 'light gray', 'dark blue', 'standout'),
        ('button select', 'white', 'dark green'),
        ('line', 'black', 'light gray', 'standout'),
        ('pg normal', 'white', 'black', 'standout'),
        ('pg complete', 'white', 'dark gray'),
        ('pg smooth', 'dark gray', 'black'),
    ]

    def __init__(self, engine, *args, **kwargs):
        self.engine = engine
        super().__init__('urwid', *args, **kwargs)

        self.loop_policy = UrwidEventLoopPolicy()
        asyncio.set_event_loop_policy(self.loop_policy)
        self.rendering_thread = threading.Thread(
            target=self.rendering_loop,
            name='gui_thread',
        )
        self.map_widget = MapWidget(engine=self.engine, user_interface=self)
        self.oxygen_widget = urwid.ProgressBar(
            'pg normal',
            'pg complete',
            0,
            100,
        )
        self.loop = urwid.MainLoop(
            urwid.Columns(
                [
                    urwid.Overlay(
                        self.map_widget,
                        urwid.SolidFill(' '),
                        align='left', width='pack',
                        valign='top', height='pack',
                    ),
                    urwid.Pile([
                        ('pack', urwid.Divider(div_char=u'\u2500')),
                        ('pack', self.oxygen_widget),
                        ('pack', urwid.Divider(div_char=u'\u2500')),
                    ]),
                ],
            ),
            self.palette,
            event_loop=urwid.AsyncioEventLoop(),
        )
        signal.signal(signal.SIGWINCH, self.loop.screen._sigwinch_handler)
        signal.signal(signal.SIGCONT, self.loop.screen._sigcont_handler)
        self.stop_flag = False
        self.engine_data = dict()
        self.player_direction = None
        self.engine_data['player_oxygen'] = self.engine.player.oxygen_level
        self.rendering_thread.start()
        asyncio.set_event_loop_policy(None)

    def _heartbeat(self):
        """
        Rendering thread realm. Tick processing handler.

        Called once every heartbeat_delay seconds by Urwid's event loop.
        """

        asyncio.set_event_loop_policy(self.loop_policy)
        self.oxygen_widget.set_completion(self.engine_data['player_oxygen'])
        self.map_widget.invalidate()
        self.loop.event_loop.alarm(self.heartbeat_delay,
                                   self._heartbeat)
        if self.stop_flag:
            raise urwid.ExitMainLoop
        asyncio.set_event_loop_policy(None)

    def rendering_loop(self):
        """
        Rendering thread realm. Event loop wrapper

        Called as soon as the UI is set up and runs Urwid's event loop.
        Makes sure _heartbeat is called as a kickstart to the rendering.
        """

        asyncio.set_event_loop_policy(self.loop_policy)
        self.loop.event_loop.alarm(self.heartbeat_delay,
                                   self._heartbeat)
        self.loop.screen.signal_init = types.MethodType(lambda x: None,
                                                        self.loop.screen)
        self.loop.screen.signal_restore = types.MethodType(lambda x: None,
                                                           self.loop.screen)
        logging.info('Rendering thread successfully started up')
        self.loop.run()
        asyncio.set_event_loop_policy(None)

    def update(self, *_, **__):
        """
        Main thread realm. UI data update method.

        Called once every engine tick by the engine itself.
        Used to update engine data to the rendering thread's engine_data dict.
        """

        if not self.rendering_thread.is_alive():
            raise RuntimeError('Rendering thread is dead.')
        self.engine_data['player_oxygen'] = self.engine.player.oxygen_level

    def get_user_actions(self, *_, **__):
        user_actions = list()
        if self.player_direction is not None:
            user_actions.append((UserAction.move, (self.player_direction,), {}))
            self.player_direction = None
        return user_actions

    def shutdown(self, *_, **__):
        asyncio.set_event_loop_policy(self.loop_policy)
        self.stop_flag = True
        self.rendering_thread.join()
        signal.signal(signal.SIGCONT, signal.SIG_DFL)
        signal.signal(signal.SIGWINCH, signal.SIG_DFL)
        logging.info('Rendering thread successfully shut down')
        asyncio.set_event_loop_policy(None)

__all__ = ['UrwidUserInterface']

#  vim: set tabstop=4 shiftwidth=4 expandtab autoindent :
