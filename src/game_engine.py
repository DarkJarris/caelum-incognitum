import asyncio
import datetime
import logging
import shelve
import math
from game_map import *
from atmospherics import *
from ui.urwid import UrwidUserInterface, UserAction


class GameEngine(object):
    _instance = None
    heartbeat_delay = 0.04

    @staticmethod
    def get_instance():
        if '_instance' not in GameEngine.__dict__.keys()\
                or GameEngine._instance is None:
            raise RuntimeError('GameEngine manipulated before instanciation.')
        return GameEngine._instance

    @staticmethod
    def set_instance(new_instance):
        if '_instance' in GameEngine.__dict__.keys()\
                and GameEngine._instance is not None:
            raise TypeError('Only a single instance of GameEngine may exist at any given time')
        GameEngine._instance = new_instance

    def __init__(self, savegame_path='./savegame.cisav'):
        GameEngine.set_instance(self)
        self.savegame = shelve.open(savegame_path)
        self.game_map = 'game_map' in self.savegame and self.savegame['game_map']\
            or GameMap(17, 17, seed=680)

        from player import Player
        self.player = 'player' in self.savegame and self.savegame['player']\
            or Player(Pos(8, 8))
        self.tick_number = 'tick_number' in self.savegame and self.savegame['tick_number']\
            or 0
        self.last_heartbeat = datetime.datetime.now()
        self.ticks_since_last_heartbeat = 0

        asyncio.set_event_loop(asyncio.SelectorEventLoop())
        self.loop = asyncio.get_event_loop()
        self.user_interface = UrwidUserInterface(self)

    def _sync_save(self):
        self.savegame['game_map'] = self.game_map
        self.savegame['player'] = self.player
        self.savegame['tick_number'] = self.tick_number
        logging.info('Saving game')
        self.savegame.sync()

    def process_user_actions(self, user_actions):
        def process_player_move(direction):
            if direction == 0:
                self.player.move(0, -1)
            elif direction == 90:
                self.player.move(1, 0)
            elif direction == 180:
                self.player.move(0, 1)
            elif direction == 270:
                self.player.move(-1, 0)

        for action, args, kwargs in user_actions:
            if action is UserAction.move:
                process_player_move(*args, **kwargs)


    def _heartbeat(self):
        self.ticks_since_last_heartbeat = 1
        self.tick_number = self.tick_number + math.floor(self.ticks_since_last_heartbeat)
        if self.ticks_since_last_heartbeat >= 1:
            self.player.heartbeat()
            self.game_map.heartbeat()
            self.last_heartbeat = datetime.datetime.now()
            self.user_interface.update()
            self.process_user_actions(self.user_interface.get_user_actions())
        self.loop.call_later(self.heartbeat_delay,
                             self._heartbeat)

    def run(self):
        self.loop.call_later(self.heartbeat_delay,
                             self._heartbeat)
        self._sync_save()
        self.last_heartbeat = datetime.datetime.now()
        self.ticks_since_last_heartbeat = 0
        try:
            self.loop.run_forever()
        finally:
            self.user_interface.shutdown()
            self._sync_save()

__all__ = ['GameEngine']
