#!/bin/env python3
# -*- coding: utf-8 -*-

from binascii import hexlify
import paramiko
import socket

HOST_KEY = paramiko.RSAKey(filename='test_rsa.key')

class Server(paramiko.ServerInterface):
    def check_auth_publickey(self, username, key):
        if username != 'caelum':
            return paramiko.AUTH_FAILED
        print(username, hexlify(key.get_fingerprint()).upper())
        return paramiko.AUTH_SUCCESSFUL

    def get_allowed_auths(self, username):
        if username == 'caelum':
            return 'publickey'
        return ''

SOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
SOCK.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
SOCK.bind(('', 2200))
SOCK.listen(5)
CLIENT, ADDR = SOCK.accept()
TRANSPORT = paramiko.Transport(CLIENT)
TRANSPORT.add_server_key(HOST_KEY)
SERVER = Server()
TRANSPORT.start_server(server=SERVER)
while True:
    CHAN = TRANSPORT.accept()
    if CHAN is None:
        continue
    CHAN.close()

#  vim: set tabstop=4 shiftwidth=4 expandtab autoindent :
