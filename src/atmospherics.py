import math

class Gas(object):
    name = 'Unknown gas'
    symbol = '?'
    heat_capacity = 0.  # Isobaric molar heat capacity, in J.mol⁻¹.K⁻¹
    molar_mass = 0.  # Molar mass in g.mol⁻¹


class Oxygen(Gas):
    name = 'Oxygen'
    symbol = 'O2'
    heat_capacity = 29.38
    molar_mass = 2 * 31.9988


class Nitrogen(Gas):
    name = 'Nitrogen'
    symbol = 'N2'
    heat_capacity = 29.12
    molar_mass = 2 * 28.013


class CarbonDioxyde(Gas):
    name = 'Carbon Dioxyde'
    symbol = 'CO2'
    heat_capacity = 36.94
    molar_mass = 1 * 12.0107 + 2 * 31.9988


class GasMixture(object):
    gas_constant = 8.3144598

    def __init__(self, volume=0., temperature=294.15, gas_mixture=tuple()):
        self.mixture = {}
        if volume < 0:
            raise ValueError('Volume in Liters can not be negative')
        self.volume = volume  # In Liters
        if temperature < 0:
            raise ValueError('Absolute temperature in Kelvin can not be negative')
        self.temperature = temperature  # In Kelvin, 294.15 K = 21°C
        for gas_class, quantity in gas_mixture:
            self.add_gas(gas_class, quantity, temperature)

    def __mul__(self, ratio):
        volume = self.volume * ratio
        new_mixture = GasMixture(volume=volume, temperature=self.temperature)

        for gas_class, quantity in self.mixture.items():
            new_mixture.add_gas(gas_class, quantity * ratio, self.temperature)

        return new_mixture

    @property
    def temperature(self):
        return self._temperature

    @temperature.setter
    def temperature(self, new_value):
        self._temperature = new_value

    @property
    def total_quantity(self):
        return sum(self.mixture.values())

    @property
    def pressure(self):
        # As per https://en.wikipedia.org/wiki/Ideal_gas_law
        converted_volume = self.volume / 1000  # One cubic meter == 1000 liters
        return self.total_quantity * self.gas_constant * self.temperature\
            / converted_volume

    def gas_ratio(self, gas_class):
        if self.total_quantity == 0:
            return 0
        return self.mixture.get(gas_class, 0) / self.total_quantity

    @property
    def molar_mass(self):
        return sum(
            [gas_class.molar_mass * self.gas_ratio(gas_class)
             for gas_class in self.mixture.keys()]
        )

    @property
    def density(self):
        mixture_mass = self.molar_mass * self.total_quantity
        return mixture_mass / self.volume

    def add_gas(self, gas_class, quantity, temperature=None):
        if quantity > 0 and abs(quantity) <= 0.00001:
            return
        if temperature is not None:
            # Here we consider that temperature is equalized instantly.

            if self.total_quantity == 0:
                ratio = 1
            else:
                ratio = quantity / self.total_quantity
            self.temperature = (self.temperature + temperature * ratio) / (1 + ratio)

        self.mixture[gas_class] = self.mixture.get(gas_class, 0.) + quantity

    def remove_gas(self, gas_class, quantity):
        floored_quantity = min(self.mixture.get(gas_class, 0.), quantity)
        self.add_gas(gas_class, -floored_quantity)
        if self.mixture.get(gas_class, 0.) <= 0.00001:
            del self.mixture[gas_class]

    def inject_mixture(self, gas_mixture):
        for gas_class, quantity in gas_mixture.mixture.items():
            self.add_gas(gas_class, quantity, gas_mixture.temperature)

    def remove_mixture(self, volume):
        volume = min(volume, self.volume)
        new_mixture = GasMixture(volume=volume, temperature=self.temperature)
        ratio = volume / self.volume

        for gas_class, quantity in list(self.mixture.items()):
            self.remove_gas(gas_class, quantity * ratio)
            new_mixture.add_gas(gas_class, quantity * ratio, self.temperature)

        return new_mixture

    def heat(self, energy):
        # Here we greatly simplify and we consider the heat capacity to be
        # constant for a given gas. Feel free to implement a more realistic
        # model if you figure this out.

        temperature_increase = 0
        for gas_class, quantity in self.mixture.items():
            weighted_energy = self.gas_ratio(gas_class) * energy
            temperature_increase = temperature_increase +\
                1 / gas_class.heat_capacity * quantity / weighted_energy
        self.temperature = self.temperature + temperature_increase

    def equalize_gas(self, neighbours):
        # Thanks to Breith for explaining the sciency stuff that this function
        # makes use of.

        if self.total_quantity == 0:
            return [0 for _ in neighbours]

        neighbours = [
            (gas_mixture, common_surface)
            for gas_mixture, common_surface in neighbours
        ]

        differentials = list()
        for gas_mixture, _ in neighbours:
            if gas_mixture is None:
                differentials.append(0)
            else:
                differentials.append(self.pressure - gas_mixture.pressure)

        total_differential = sum(differentials)
        if total_differential == 0:
            return [0 for _ in neighbours]
        differentials = [(differential, differential / total_differential)
                         for differential in differentials]
        density = self.density
        flows = []

        for (gas_mixture, common_surface),\
                (differential, differential_ratio) in zip(neighbours, differentials):
            if differential < 0 or gas_mixture is None:
                # We only flow out, not in
                flows.append(0)
                continue
            flow = math.sqrt(2 * differential / density) * common_surface * differential_ratio
            if flow < 0.000001:
                flows.append(0)
                continue
            flow = min(flow, self.volume / 1000)
            flows.append(flow)
        return flows

    def __str__(self):
        gas_mix = dict([(gas.name, self.gas_ratio(gas)) for gas in self.mixture.keys()])
        return 'GasMixture:{temperature} K|{pressure} Pa|{volume} L|{total_quantity} mol|{gas_mix}'.format(
            temperature=self.temperature,
            pressure=self.pressure,
            volume=self.volume,
            total_quantity=self.total_quantity,
            gas_mix=gas_mix,
        )

    def __repr__(self):
        return str(self)

__all__ = ['Oxygen', 'Nitrogen', 'CarbonDioxyde', 'GasMixture']
